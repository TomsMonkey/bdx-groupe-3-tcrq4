# image de base
FROM nginx
# Update de l'image
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl && \
    apt-get install -y git
# Installation de Mocha
RUN apt install mocha -y
# Installation de VIM / Nano
RUN apt install vim -y
RUN rm -Rf /usr/share/nginx/html/*
RUN git clone https://github.com/PetitSinge/projetibwebsite.git /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
